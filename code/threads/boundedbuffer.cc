#include "boundedbuffer.h"
#include "string.h"


BoundedBuffer::BoundedBuffer(int maxsize){
	lock = new Lock("buffer lock");
	bufferEmpty = new Condition("bufferEmpty");
	bufferFull = new Condition("bufferFull");
	buffer = (void*)AllocBoundedArray(sizeof(void*) * maxsize);
	bufferSize = maxsize;
	count = 0;
	nextin = nextout = 0;
}

BoundedBuffer::~BoundedBuffer(){
	Close();
}

void BoundedBuffer::Close() {
	DeallocBoundedArray((char *)buffer, bufferSize);
	delete lock;
	delete bufferEmpty;
	delete bufferFull;
}

int BoundedBuffer::Read(void* data, int size){
	int i;
	lock->Acquire();
	//memcpy(data, buffer, size);
	//cout << data << endl;
	while(count == 0)
		bufferFull->Wait(lock);
	for(i = 0; i < size; i++) {
		while(count == 0)
			bufferFull->Wait(lock);
		*((char *)data + i) = *((char *)buffer + nextout);
		nextout = (nextout + 1) % bufferSize;
		count--;
		bufferEmpty->Signal(lock);
	}
	lock->Release();
	return i;
}

int BoundedBuffer::Write(void *data, int size) {

	int j;
	lock->Acquire();
	while(count == bufferSize)
		bufferEmpty->Wait(lock);
	
	for(j = 0; j < size; j++) {
		while(count == bufferSize)
			bufferEmpty->Wait(lock);
		*((char *)buffer + nextin) = *((char *)data + j);
		nextin = (nextin + 1) % bufferSize;
		count++;
		bufferFull->Signal(lock);
	}
	lock->Release();

	return j;
}
