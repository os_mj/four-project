#include "synch.h"

class BoundedBuffer {
public:
	BoundedBuffer(int maxsize);
	~BoundedBuffer();
	int Read(void* data, int size);
	int Write(void* data, int size);
	void Close();
private:
	void *buffer;
	int count;
	int nextin;
	int nextout;
	int bufferSize;
	
	Lock *lock;
	Condition *bufferEmpty;
	Condition *bufferFull;
};



